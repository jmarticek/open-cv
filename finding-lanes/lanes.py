import cv2
import numpy as np
import matplotlib.pyplot as plt

# Funkcia na zistenie hrán
#   Prevedie image na greyscale
#   Aplikuje Gaussov filter
#   Aplikuje Cannyho funkciu, ktorá vypočíta a zaznačí gradienty (ich rozdiely v jase) každých 2 susedných pixelov
#       (Táto funkcia už aplikuje noise reduction a teda gauss je nadbytočný)
def canny(image):
    gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    blur = cv2.GaussianBlur(gray, (5,5), 0)
    return cv2.Canny(blur, 50, 150)

# Funkcia na zobrazenie čiar
#   Len prevedie usporiadané dvojice bodov na čiary
def display_lines(image, lines):
    line_image = np.zeros_like(image)
    if lines is not None:
        for x1, y1, x2, y2 in lines:
            cv2.line(line_image, (x1, y1), (x2, y2), (255, 0, 0), 10)
    return line_image

# Táto funkcia nám vystrihne z obrázka región, ktorý nás zaujíma
#   Región je zadefinovaný ako trojuholník s prevnými číslami konkrétnymi pre daný obrázok
def interestRegion(image):
    height = image.shape[0]
    triangles = np.array([
        [(200, height), (1100, height), (550, 250)],
    ])
    mask = np.zeros_like(image)
    cv2.fillPoly(mask, triangles, 255)
    masked_image = cv2.bitwise_and(image, mask)
    return masked_image

# Prevod parametrického vyjadrenia priamky na 2 body ktoré ležia na nej
def make_coordinates(image, line_parameters):
    slope, intercept = line_parameters
    y1 = image.shape[0]
    y2 = int(y1*(3/5))
    x1 = int((y1 - intercept) / slope) 
    x2 = int((y2 - intercept) / slope)
    return np.array([x1, y1, x2, y2])

# Triedime naše nájdené priamky do dvoch (buď ľavej - stredovej čiary) alebo pravej čiary
def average_slope_intercept(image, lines):
    left_fit = []
    right_fit = []
    for line in lines:
        x1, y1, x2, y2 = line.reshape(4)
        parameters = np.polyfit((x1, x2), (y1, y2), 1)
        slope = parameters[0]
        offset = parameters[1]
        if slope < 0:
            left_fit.append((slope, offset))
        else:
            right_fit.append((slope, offset))
    left_line = make_coordinates(image, np.average(left_fit, axis=0))
    right_line = make_coordinates(image, np.average(right_fit, axis=0))
    return np.array([left_line, right_line])

# Načítame obrázok
image = cv2.imread('test_image.jpg')
lane_image = np.copy(image)

# Aplikujeme Cannyho funkciu - po tomto úkone máme obrázok len s hranami
cannyIm = canny(image)

# Orežeme
cropped = interestRegion(cannyIm)

# Aplikujeme Houghovu transformáciu (zisťuje priamky zo série bodov) viz IZU a.i.
lines = cv2.HoughLinesP(cropped, 2, np.pi/180, 100, np.array([]), minLineLength=40, maxLineGap=5)

# Pospájame nájdené priamky do jednej (resp. dvoch)
averaged_lines = average_slope_intercept(lane_image, lines)

# Vykreslíme prázdny obrázok s čiarami
line_image = display_lines(lane_image, averaged_lines)

# Skombinujeme originál obrázok a čiary
combo_image = cv2.addWeighted(lane_image, 0.8, line_image, 1, 1)

# Render
cv2.imshow('result', combo_image)
cv2.waitKey(0)
